FROM ubuntu:22.04
WORKDIR /
COPY requirements.txt .
ADD https://raw.githubusercontent.com/fsaintjacques/semver-tool/master/src/semver /usr/local/bin/semver

RUN chmod +x /usr/local/bin/semver && \
  apt-get update && \
  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --assume-yes \
  apt-transport-https \
  ca-certificates \
  lsb-release \
  python3-pip \
  curl \
  ssh \
  git \
  software-properties-common && \
  echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list && \
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/trusted.gpg.d/docker.asc && \
  apt-get update && \
  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --assume-yes docker-ce && \
  pip install --no-cache-dir -r requirements.txt && \
  apt-get autoremove --assume-yes && \
  apt-get clean && \
  rm --force --recursive /var/lib/apt/lists/* /tmp/* /var/tmp/*
